@extends('welcome')

@section('title')
    Список url
@endsection

@section('content')
    <a href="{!! route('records_create') !!}" class="btn btn-primary">Добавить новый url</a>
    <hr>
    @if($records->count() > 0)
        <table class="table-borderless">
            @foreach($records as $record)
                <tr>
                    <td>
                        <a href="{!! route('records_expand', ['hash'=>$record->hash]) !!}" target="_blank">
                            {!! env('APP_URL') !!}/r/{{ $record->hash }}
                        </a>
                    </td>
                    <td>
                        <a href="{!! route('records_edit', ['id'=>$record->id]) !!}" class="btn btn-sm btn-outline-info"
                           title="Редактировать">
                            <i class="fa fa-pencil"></i>
                        </a>
                        <button type="button" class="btn btn-sm btn-outline-primary removeRecord" title="Удалить" data-record="{{ $record->id }}">
                            <i class="fa fa-trash"></i>
                        </button>

                    </td>
                </tr>
            @endforeach
        </table>
    @else
        <p>Нет записей.</p>
    @endif
@endsection

@section('scripts')
    <script>
        $(document).ready(function () {
            $('.removeRecord').click(function (e) {
                if (confirm('Вы действительно хотите удалить запись?')) {
                    var btn = $(this);
                    var id = btn.data('record');

                    $.ajax({
                        url: '/records/'+id,
                        type: 'DELETE',
                        data: {
                            '_token': '{{ csrf_token() }}'
                        },
                        success: function(result) {
                            var data = $.parseJSON(result);
                            if (data.status=='success') {
                                btn.closest('tr').remove();
                            }
                        }
                    });

                }
            });
        });
    </script>
@endsection

