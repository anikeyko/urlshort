@extends('welcome')

@section('title')
    Редактирование
@endsection

@section('content')
    <form action="{!! route('records_update', ['id'=>$record->id]) !!}" class="form-horizontal" method="POST">
        {{ csrf_field() }}
        {{ method_field('PATCH') }}
        @if($errors->count())
            <div class="alert alert-danger" role="alert">
                <ul>
                    @foreach($errors->all() as $message)
                        <li>{{ $message }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <div class="form-group">
            <label for="url">Введите URL</label>
            <input type="url" required name="url" class="form-control" id="url" placeholder="https://google.com"
                   value="{{ $record->url }}">
        </div>
        <button type="submit" class="btn btn-primary">Сохранить</button>
    </form>
@endsection
