@extends('welcome')

@section('title')
    Сократить url
@endsection

@section('content')
    <form action="{!! route('records_store') !!}" class="form-horizontal" method="POST">
        {{ csrf_field() }}
        @if($errors->count())
            <div class="alert alert-danger" role="alert">
                <ul>
                    @foreach($errors->all() as $message)
                        <li>{{ $message }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <div class="form-group">
            <label for="url">Введите URL</label>
            <input type="url" required name="url" class="form-control" id="url" placeholder="https://google.com">
        </div>
        <button type="submit" class="btn btn-primary">Сократить</button>
    </form>
@endsection
