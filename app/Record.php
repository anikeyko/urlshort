<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Hash;
use function foo\func;

class Record extends Model
{
    protected $fillable = [
        'url'
    ];

    protected static function boot()
    {
        parent::boot();

        static::creating(function ($instance) {
            $instance->hash = substr(md5($instance->url), 0, 10);
        });
    }


    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
