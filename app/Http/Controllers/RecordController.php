<?php

namespace App\Http\Controllers;

use App\Http\Requests\RecordRequest;
use App\Record;
use Illuminate\Http\Request;

class RecordController extends Controller
{

    /**
     * Список всех записей
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $records = Record::all();

        return view('records.index', compact('records'));
    }

    /**
     * Форма создания новой записи
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create(Request $request)
    {
        return view('records.create');
    }

    /**
     * Сохранение новой записи
     * @param RecordRequest $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(RecordRequest $request)
    {
        $record = new Record($request->all());

        if ($record->save()) {
            return redirect(route('records'));
        }
    }

    /**
     * Форма редактирования записи
     * @param Request $request
     * @param int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Request $request, int $id)
    {
        $record = Record::findOrFail($id);

        return view('records.edit', compact('record'));
    }

    /**
     * Обновление записи
     * @param RecordRequest $request
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(RecordRequest $request, int $id)
    {
        $record = Record::findOrFail($id);

        $record->update($request->only(['url']));

        return redirect(route('records'));
    }

    public function delete(Request $request, int $id)
    {
        $record = Record::findOrFail($id);
        $result = $record->delete();
        echo json_encode([
            'status' => ($result) ? 'success' : 'error'
        ]);
    }

    /**
     * Открытие url
     * @param Request $request
     * @param string $hash
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function expand(Request $request, string $hash)
    {
        $record = Record::where('hash', $hash)->firstOrFail();

        return redirect($record->url);
    }
}
