<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/records', 'RecordController@index')->name('records');
Route::post('/records', 'RecordController@store')->name('records_store');
Route::get('/records/create', 'RecordController@create')->name('records_create');
Route::get('/records/{id}/edit', 'RecordController@edit')->name('records_edit');
Route::patch('/records/{id}', 'RecordController@update')->name('records_update');
Route::delete('/records/{id}', 'RecordController@delete')->name('records_delete');

Route::get('/r/{hash}', 'RecordController@expand')->name('records_expand');
